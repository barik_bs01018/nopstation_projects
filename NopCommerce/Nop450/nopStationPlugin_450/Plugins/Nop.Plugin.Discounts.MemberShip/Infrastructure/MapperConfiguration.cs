﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Discounts.MemberShip.Domains;
using Nop.Plugin.Discounts.MemberShip.Models;

namespace Nop.Plugin.Discounts.MemberShip.Infrastructure
{
    public class MapperConfiguration : Profile, IOrderedMapperProfile
    {
        public MapperConfiguration()
        {
            CreateMap<MemberShipDiscountDomain, MemberShipModel>();
            CreateMap<MemberShipModel, MemberShipDiscountDomain>();
        }
        public int Order => 1;
    }
}
