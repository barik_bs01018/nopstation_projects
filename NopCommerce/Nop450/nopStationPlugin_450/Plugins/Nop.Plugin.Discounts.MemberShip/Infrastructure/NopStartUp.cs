﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Discounts.MemberShip.Factories;
using Nop.Plugin.Discounts.MemberShip.Services;


namespace Nop.Plugin.Discounts.MemberShip.Infrastructure
{
    public class NopStartUp : INopStartup
    {
        public int Order => 100;
       
        public void Configure( IApplicationBuilder application)
        {

        }
        public void ConfigureServices(IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<IMemberShipModelFactory, MemberShipModelFactory>();
        }

    }
}
