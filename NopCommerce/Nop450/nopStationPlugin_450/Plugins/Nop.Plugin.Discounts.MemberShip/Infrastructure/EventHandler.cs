﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Orders;
using Nop.Plugin.Discounts.MemberShip.Services;

namespace Nop.Plugin.Discounts.MemberShip.Infrastructure
{
    public class EventHandler:IConsumer<OrderPaidEvent>,IConsumer<CustomerRegisteredEvent>
    {
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly IConfigurationService _configurationService;
        public EventHandler(ICustomerService customerService,IOrderService orderService, IConfigurationService configurationService)
        {
            _customerService = customerService;
            _orderService = orderService;
            _configurationService= configurationService;
        }

        public async Task HandleEventAsync(OrderPaidEvent eventMessage)
        {
            var order = eventMessage.Order;
            var customer = await _customerService.GetCustomerByIdAsync(order.CustomerId);

            var allorders = (await _orderService.SearchOrdersAsync(customerId: customer.Id));

            var customerRoleIds = await _customerService.GetCustomerRoleIdsAsync(customer);

            var totalOrders = allorders.Count();
            decimal totalCash=0;
            foreach (var x in allorders)
            {
                totalCash += x.OrderTotal;
            }

            var memberShipRoles = await _configurationService.GetAllMemberShipsAsync();
            bool addRole=false;
            int roleId = 0;

            foreach(var role in memberShipRoles)
            {
                var customerRole = await _customerService.GetCustomerRoleByIdAsync(role.MemberShipRoleId);
                if(await _customerService.IsInCustomerRoleAsync(customer,customerRole.SystemName))
                {
                    continue;
                }

                addRole = false;
                if(role.IsCashCount)
                {
                    if(totalCash >=role.TargetCashCount)
                    {
                        addRole = true;
                        roleId = role.MemberShipRoleId;
                        //break;
                    }
                }
                else
                {
                    if(totalOrders >=role.TargetOrderCount)
                    {
                        addRole = true;
                        roleId = role.MemberShipRoleId;
                        //break;
                    }
                }
                if (addRole)
                {
                    await _customerService.AddCustomerRoleMappingAsync(
                        new CustomerCustomerRoleMapping
                        {
                            CustomerId = customer.Id,
                            CustomerRoleId = roleId
                        });
                }
            }

          
            
        }

        public Task HandleEventAsync(CustomerRegisteredEvent eventMessage)
        {
            throw new NotImplementedException();
        }
    }
}
