﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data;
using Nop.Plugin.Discounts.MemberShip.Domains;
using Nop.Plugin.Discounts.MemberShip.Models;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Services.Customers;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;

namespace Nop.Plugin.Discounts.MemberShip.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IRepository<MemberShipDiscountDomain> _repoConfig;
        private readonly ICustomerService _customerService;


        public ConfigurationService(IRepository<MemberShipDiscountDomain> repoConfig, ICustomerService customerService)
        {
            _repoConfig = repoConfig;
            _customerService = customerService;
        }

        public async Task<bool> CreateMemberShipCriteriaAsync (MemberShipModel viewModel)
        {
            var model = viewModel.ToEntity<MemberShipDiscountDomain>();
            try
            {
                await _repoConfig.InsertAsync(model);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public async Task<List<DiscountCustomerRoleModel>> GetMemberShipRolesAsync ()
        {
            var customerRoles = ((await _customerService.GetAllCustomerRolesAsync()).Select(x => new DiscountCustomerRoleModel()
            {
                CustomerRoleId = x.Id,
                CustomerRoleName = x.Name,
            })).ToList();

            return customerRoles;

        }

        public async Task<IPagedList<MemberShipDiscountDomain>> GetAllMemberShipsAsync(string memberShipName=null)
        {
            var memberShips = await _repoConfig.Table.ToPagedListAsync(pageIndex: 0, pageSize: int.MaxValue);

            if(memberShipName != null)
            {
                memberShips = await _repoConfig.Table.Where(x=>x.MemberShipName == memberShipName).
                    ToPagedListAsync(pageIndex: 0, pageSize: int.MaxValue);

            }

            return memberShips;
        }

        public async Task DeleteProductsAsync(IList<MemberShipDiscountDomain> memberShips)
        {
            await _repoConfig.DeleteAsync(memberShips);
        }

        public async Task<IList<MemberShipDiscountDomain>> GetMemberShipsByIdsAsync(int[] memberShipIds)
        {
            return await _repoConfig.GetByIdsAsync(memberShipIds);
        }

        public async Task<MemberShipDiscountDomain> GetMemberShipByIdAsync(int id)
        {
            return await _repoConfig.GetByIdAsync(id);
        }

        public async Task UpdateMemberShipAsync(MemberShipDiscountDomain model)
        {
            await _repoConfig.UpdateAsync(model);
        }

        public async Task<List<MemberShipDiscountDomain>> GetAllMemberShipsAsync()
        {           
            return (await _repoConfig.GetAllAsync(entity=>entity.Where(x=>x.Id>0))).ToList();
        }

        public MemberShipModel GetMemberShipByCustomerRoleId(int id)
        {
            var domain = _repoConfig.Table.FirstOrDefault(x => x.MemberShipRoleId == id);
            var model = new MemberShipModel();
            if(domain != null)  
              model = domain.ToModel<MemberShipModel>();
            return model;
        }
    }
}
