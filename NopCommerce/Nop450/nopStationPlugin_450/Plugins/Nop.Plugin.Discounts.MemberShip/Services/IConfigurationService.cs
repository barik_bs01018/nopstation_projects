﻿using System.Threading.Tasks;
using Nop.Plugin.Discounts.MemberShip.Models;
using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Discounts.MemberShip.Domains;

namespace Nop.Plugin.Discounts.MemberShip.Services
{
    public interface IConfigurationService
    {
        Task<bool> CreateMemberShipCriteriaAsync(MemberShipModel viewModel);
        Task<List<DiscountCustomerRoleModel>> GetMemberShipRolesAsync();
        Task<IPagedList<MemberShipDiscountDomain>> GetAllMemberShipsAsync(string memberShipName);
        Task DeleteProductsAsync(IList<MemberShipDiscountDomain> memberShips);

        Task<IList<MemberShipDiscountDomain>> GetMemberShipsByIdsAsync(int[] memberShipIds);
        Task<MemberShipDiscountDomain> GetMemberShipByIdAsync(int id);

        Task UpdateMemberShipAsync(MemberShipDiscountDomain model);

        Task<List<MemberShipDiscountDomain>> GetAllMemberShipsAsync();

        MemberShipModel GetMemberShipByCustomerRoleId(int id);
    }
}