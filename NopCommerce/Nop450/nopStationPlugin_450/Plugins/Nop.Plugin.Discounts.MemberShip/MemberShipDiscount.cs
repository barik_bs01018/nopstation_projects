﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Services.Cms;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Discounts.MemberShip
{
    public class MembershipDiscount: BasePlugin, IWidgetPlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;

        public MembershipDiscount(IWebHelper webHelper, ILocalizationService localizationService)
        {
            _webHelper = webHelper;
            _localizationService = localizationService;
        }

        public bool HideInWidgetList => false;

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsDiscountsMemberShip";
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { AdminWidgetZones.CustomerRoleDetailsBottom });
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/DiscountsMemberShip/Configure";
        }

        public override async Task InstallAsync()
        {
            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Discounts.MemberShip.Fields.CustomerRole"] = "Membership Name",
                ["Plugins.Discounts.MemberShip.Fields.CustomerRole.Hint"] = "Required Membership Name(unique)",
                ["Plugins.Discounts.MemberShip.Fields.TargetOrderCount"] = "Target Order",
                ["Plugins.Discounts.MemberShip.Fields.TargetOrderCount.Hint"] = "Required Target Order",
                ["Plugins.Discounts.MemberShip.Fields.TargetCashCount"] = "Target Cash",
                ["Plugins.Discounts.MemberShip.Fields.TargetCashCount.Hint"] = "Required Target Cash",
                ["Plugins.Discounts.MemberShip.Fields.IsActive"] = "Is Avtive",
                ["Plugins.Discounts.MemberShip.Fields.SelectTargetCashCount"] = " Select Cash",
                ["Plugins.Discounts.MemberShip.Fields.PluginName"] = "MemberShip Discount",
                ["Plugins.Discounts.MemberShip.Fields.Active"] = "Active",
                ["Plugins.Discounts.MemberShip.Configuration.Fields.TargetCashCount.Required"] ="Target Cash must be Greater than 0",
                ["Plugins.Discounts.MemberShip.Configuration.Fields.TargetOrderCount.Required"] = "Target Order must be Greater than 0",

            });
            await base.InstallAsync();
        }

        public override async Task UpdateAsync(string currentVersion, string targetVersion)
        {
            if (currentVersion == "1.45" && targetVersion == "1.46")
            {
                await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
                {
                    ["Plugins.Discounts.MemberShip.Fields.CustomerRole"] = "Membership Name",
                    ["Plugins.Discounts.MemberShip.Fields.CustomerRole.Hint"] = "Required Membership Name(unique)",
                    ["Plugins.Discounts.MemberShip.Fields.TargetOrderCount"] = "Target Order",
                    ["Plugins.Discounts.MemberShip.Fields.TargetOrderCount.Hint"] = "Required Target Order",
                    ["Plugins.Discounts.MemberShip.Fields.TargetCashCount"] = "Target Cash",
                    ["Plugins.Discounts.MemberShip.Fields.TargetCashCount.Hint"] = "Required Target Cash",
                    ["Plugins.Discounts.MemberShip.Fields.IsActive"] = "Is Avtive",
                    ["Plugins.Discounts.MemberShip.Fields.SelectTargetCashCount"] = " Select Cash",
                    ["Plugins.Discounts.MemberShip.Fields.PluginName"] = "MemberShip Discount",
                    ["Plugins.Discounts.MemberShip.Fields.Active"] = "Active",
                    ["Plugins.Discounts.MemberShip.Configuration.Fields.TargetCashCount.Required"] = "Target Cash must be Greater than 0",
                    ["Plugins.Discounts.MemberShip.Configuration.Fields.TargetOrderCount.Required"] = "Target Order must be Greater than 0",
                });
            }
            await base.UpdateAsync(currentVersion, targetVersion);
        }

    }
}