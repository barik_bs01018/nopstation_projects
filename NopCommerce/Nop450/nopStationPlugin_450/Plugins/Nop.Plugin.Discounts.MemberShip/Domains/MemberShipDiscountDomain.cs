﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Discounts.MemberShip.Domains
{
    public class MemberShipDiscountDomain : BaseEntity
    {
        public int MemberShipRoleId { get; set; }

        public string MemberShipName { get; set; }
        public int TargetOrderCount { get;set; }
        public bool IsActive { get; set; }
        public decimal TargetCashCount { get; set; }

        public bool IsCashCount { get; set; }


    }
}
