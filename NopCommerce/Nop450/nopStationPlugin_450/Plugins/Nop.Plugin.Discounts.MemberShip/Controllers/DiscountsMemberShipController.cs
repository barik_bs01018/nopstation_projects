﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Discounts.MemberShip.Models;
using Nop.Plugin.Discounts.MemberShip.Services;
using Nop.Web.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Discounts.MemberShip.Factories;
using Nop.Services.Customers;
using Nop.Plugin.Discounts.MemberShip.Domains;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;

namespace Nop.Plugin.Discounts.MemberShip.Controllers
{
    public class DiscountsMemberShipController : BaseAdminController
    {
        private readonly IConfigurationService _configService;
        private readonly IMemberShipModelFactory _memberShipFactory;
        private readonly ICustomerService _customerService;

        public DiscountsMemberShipController(IConfigurationService configService,
            IMemberShipModelFactory memberShipFactory, ICustomerService customerService)
        {
            _configService = configService;
            _memberShipFactory = memberShipFactory;
            _customerService = customerService;
        }

        public  async Task<IActionResult> Configure()
        {
          
            var customerRoles = await _configService.GetMemberShipRolesAsync();
            ViewBag.MemberShipRoles = new SelectList(customerRoles, "CustomerRoleId", "CustomerRoleName");

            var model = await _memberShipFactory.PrepareMemberShipSearchModelAsync(new MemberShipSearchModel());
            return View("~/Plugins/Discounts.MemberShip/Views/Configure.cshtml", model);

        }
        [HttpPost]
        public  async Task<IActionResult>MemberShipList(MemberShipSearchModel searchModel)
        {
            var model =await _memberShipFactory.PrepareMemberShipListModelAsync(searchModel);
            return Json(model);
        }
        public async Task< IActionResult> Create()
        {
            var model = new MemberShipModel();
            var customerRoles = await _configService.GetMemberShipRolesAsync();
            ViewBag.MemberShipRoles = new SelectList(customerRoles, "CustomerRoleId", "CustomerRoleName");
            return View("~/Plugins/Discounts.MemberShip/Views/Create.cshtml", model);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MemberShipModel model)
        {
            var basemodel = new MemberShipModel();
            var customerRoles = await _configService.GetMemberShipRolesAsync();

            if (ModelState.IsValid)
            {
                var customerRoleName = await _customerService.GetCustomerRoleByIdAsync(model.MemberShipRoleId);

                model.MemberShipName = customerRoleName.Name;
                model.IsActive = customerRoleName.Active;
                bool res=await _configService.CreateMemberShipCriteriaAsync(model);

                if(!res)
                {
                    ViewBag.MemberShipRoles = new SelectList(customerRoles, "CustomerRoleId", "CustomerRoleName");
                    ModelState.AddModelError("MemberShipRoleId", "This role already exist!!");
                    return View("~/Plugins/Discounts.MemberShip/Views/Create.cshtml", basemodel);
                }


                return RedirectToAction("Configure");
            }

            
            ViewBag.MemberShipRoles = new SelectList(customerRoles, "CustomerRoleId", "CustomerRoleName");
            return View("~/Plugins/Discounts.MemberShip/Views/Create.cshtml", model);

        }
        [HttpPost]
        public async Task<IActionResult> DeleteSelected(ICollection<int> selectedIds)
        {
            if (selectedIds == null || selectedIds.Count == 0)
                return NoContent();
            await _configService.DeleteProductsAsync((await _configService.GetMemberShipsByIdsAsync(selectedIds.ToArray())).ToList());
            
            return Json(new { Result = true });
        }

        public async Task<IActionResult> Edit(int id)
        {
            var data = await _configService.GetMemberShipByIdAsync(id);

            if (data == null)
            {
                return RedirectToAction("Configure");
            }
            var model = data.ToModel<MemberShipModel>();


            return View("~/Plugins/Discounts.MemberShip/Views/Edit.cshtml",model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MemberShipModel model)
        {
            if(ModelState.IsValid)
            {
                var memberShipmodel = _configService.GetMemberShipByCustomerRoleId(model.MemberShipRoleId);
                model.Id=memberShipmodel.Id;
                var data = model.ToEntity<MemberShipDiscountDomain>();

                await _configService.UpdateMemberShipAsync(data);

                
                return RedirectToAction("Configure");
            }
            return RedirectToAction("Edit",model.Id);
        }
    }
}
