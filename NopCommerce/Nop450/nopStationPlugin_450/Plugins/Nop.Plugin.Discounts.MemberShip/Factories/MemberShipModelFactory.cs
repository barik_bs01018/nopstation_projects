﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Discounts.MemberShip.Models;
using Nop.Plugin.Discounts.MemberShip.Services;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Discounts.MemberShip.Factories
{
    public class MemberShipModelFactory : IMemberShipModelFactory
    {
        private readonly IConfigurationService _configurationService;

        public MemberShipModelFactory(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        public async Task<MemberShipSearchModel> PrepareMemberShipSearchModelAsync(MemberShipSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            searchModel.SetGridPageSize();
            return searchModel;
        }

        public async Task<MemberShipListModel> PrepareMemberShipListModelAsync(MemberShipSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var memberShips = await _configurationService.GetAllMemberShipsAsync(searchModel.MemberShipName);


            var model = await new MemberShipListModel().PrepareToGridAsync(searchModel, memberShips, () =>
            {
                return memberShips.SelectAwait(async member =>
                {

                    var memberModel = member.ToModel<MemberShipModel>();

                    return memberModel;
                });
            });

            return model;
        }
    }
}
