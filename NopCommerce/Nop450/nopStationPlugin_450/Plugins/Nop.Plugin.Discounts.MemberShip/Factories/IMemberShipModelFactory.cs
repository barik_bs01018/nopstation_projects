﻿using System.Threading.Tasks;
using Nop.Plugin.Discounts.MemberShip.Models;

namespace Nop.Plugin.Discounts.MemberShip.Factories
{
    public interface IMemberShipModelFactory
    {
        Task<MemberShipListModel> PrepareMemberShipListModelAsync(MemberShipSearchModel searchModel);
        Task<MemberShipSearchModel> PrepareMemberShipSearchModelAsync(MemberShipSearchModel searchModel);
    }
}