﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Discounts.MemberShip.Models;
using Nop.Plugin.Discounts.MemberShip.Services;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Discounts.MemberShip.Components
{
    public class WidgetsDiscountsMemberShipViewComponent : NopViewComponent
    {
        private readonly IConfigurationService _configureService;

        public WidgetsDiscountsMemberShipViewComponent(IConfigurationService configureService)
        {
            _configureService = configureService;
        }
        public  IViewComponentResult  Invoke(string widgetZone, object additionalData)
        {
            var memberShipdata= new MemberShipModel();
            var customerRoleModel = (CustomerRoleModel)additionalData;
            //var model = additionalData==null ? new MemberShipModel { FromComponent = 1,MemberShipRoleId=customerRoleModel.Id}:
            var model = new MemberShipModel() {
                FromComponent=1,
                MemberShipRoleId= customerRoleModel.Id ,
            };

             memberShipdata =  _configureService.GetMemberShipByCustomerRoleId(customerRoleModel.Id);

            if(memberShipdata.MemberShipRoleId>0)
            {
                memberShipdata.FromComponent = 1;
                return View("~/Plugins/Discounts.MemberShip/Views/Edit.cshtml", memberShipdata);
            }
            else
            {
                return View("~/Plugins/Discounts.MemberShip/Views/Create.cshtml", model);
            }

            
        }
    }
}
