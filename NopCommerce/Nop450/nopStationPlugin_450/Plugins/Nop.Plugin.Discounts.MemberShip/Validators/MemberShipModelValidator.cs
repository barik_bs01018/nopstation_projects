﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Nop.Plugin.Discounts.MemberShip.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Discounts.MemberShip.Validators
{
    public class MemberShipModelValidator: BaseNopValidator<MemberShipModel>
    {
        public MemberShipModelValidator(ILocalizationService localizationService)
        {
            RuleFor(model => model.TargetCashCount)
                .GreaterThan(0).When(model => model.IsCashCount)
                .WithMessageAwait(localizationService.GetResourceAsync("Plugins.Discounts.MemberShip.Configuration.Fields.TargetCashCount.Required"));

            RuleFor(model => model.TargetOrderCount)
                .GreaterThan(0).When(model => !model.IsCashCount)
                .WithMessageAwait(localizationService.GetResourceAsync("Plugins.Discounts.MemberShip.Configuration.Fields.TargetOrderCount.Required"));
        }
       

    }
}
