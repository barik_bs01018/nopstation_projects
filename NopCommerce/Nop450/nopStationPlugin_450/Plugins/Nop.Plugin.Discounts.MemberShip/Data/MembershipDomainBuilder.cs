﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator.Builders.Create.Table;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Discounts.MemberShip.Domains;

namespace Nop.Plugin.Discounts.MemberShip.Data
{
    public class MembershipDomainBuilder: NopEntityBuilder<MemberShipDiscountDomain>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table.WithColumn(nameof(MemberShipDiscountDomain.MemberShipRoleId)).AsInt32().Unique();
        }

    }
}
