﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data.Mapping;
using Nop.Plugin.Discounts.MemberShip.Domains;

namespace Nop.Plugin.Discounts.MemberShip.Data
{
    public class BaseNameCompatibility: INameCompatibility
    {
        public Dictionary<Type, string> TableNames =>
            new Dictionary<Type, string>
            {
                {typeof(MemberShipDiscountDomain), "NS_DM_Membership"}
            };

        public Dictionary<(Type, string), string> ColumnName => new Dictionary<(Type, string), string>();

    }
}
