﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using Nop.Data.Extensions;
using Nop.Data.Migrations;
using Nop.Plugin.Discounts.MemberShip.Domains;

namespace Nop.Plugin.Discounts.MemberShip.Data
{
    [NopMigration("2022/02/23 23:00:00", "Discounts.MemberShip base schema", MigrationProcessType.Installation)]
    public class SchemaMigration: AutoReversingMigration
    {
        public override void Up()
        {
            Create.TableFor<MemberShipDiscountDomain>();
        }

    }
}
