﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;


namespace Nop.Plugin.Discounts.MemberShip.Models
{
    public record MemberShipModel:BaseNopEntityModel
    {
        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.CustomerRole")]
        public int MemberShipRoleId { get; set; }
        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.CustomerRole")]
        public string MemberShipName { get; set; }

        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.TargetOrderCount")]
        public int TargetOrderCount { get; set; }

        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.TargetCashCount")]
        public decimal TargetCashCount { get; set; }
        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.SelectTargetCashCount")]
        public bool IsCashCount { get; set; }
        [NopResourceDisplayName("Plugins.Discounts.MemberShip.Fields.IsActive")]
        public bool IsActive { get; set; }

        public int FromComponent { get; set; }
    }
}
