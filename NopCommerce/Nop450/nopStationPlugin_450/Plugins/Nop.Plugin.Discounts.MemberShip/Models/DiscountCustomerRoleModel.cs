﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Discounts.MemberShip.Models
{
    public class DiscountCustomerRoleModel
    {
        public int CustomerRoleId { get; set; }
        public string CustomerRoleName { get; set; }
    }
}
