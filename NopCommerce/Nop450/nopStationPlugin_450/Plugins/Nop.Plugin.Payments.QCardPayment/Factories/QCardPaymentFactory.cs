﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Models;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Factories
{
    public class QCardPaymentFactory : IQCardPaymentFactory
    {
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IWebHelper _webHelper;


        public QCardPaymentFactory(IOrderService orderService,
                                   IProductService productService,
                                   IWebHelper webHelper)
        {
            _orderService = orderService;
            _productService = productService;
            _webHelper = webHelper;
        }
        public async Task<PaymentUrlRequest> PreparePaymentUrlRequest(QCardPaymentSettings settings, Order orders)
        {
            var lineItems = await (await _orderService.GetOrderItemsAsync(orders.Id))
                            .SelectAwait(async oi =>
                            {
                                var product = await _productService.GetProductByIdAsync(oi.ProductId);
                                var lineItem = new LineItem()
                                {
                                    MerchantProductCode = product.Sku,
                                    Description = product.Name,
                                    Quantity = oi.Quantity,
                                    Amount = (int)product.Price * 100,

                                };
                                return lineItem;
                            }).ToListAsync();

            var requestBody = new PaymentUrlRequest()
            {
                MerchantId = settings.MerchantId,
                LoginId = settings.LoginId,
                Password = settings.Password,
                GetPaymentURLApiKey = settings.ApiKey,
                MerchantTransactionId = orders.Id.ToString(),
                TransactionAmount = Convert.ToInt32(orders.OrderTotal * 100),
                IncludeProductCodes = new ProductCode() { AllProductCode = new List<string>(){ "1_bike", "2_car", "3_bicycle" } }, // need to update
                ExcludeProductCodes = new ProductCode(), // need to update
                UrlResponse = $"{_webHelper.GetStoreLocation()}QCardPayment/PostPaymentHandler",
                DirectToUrlResponse = true,
                LineItem = new LineItems() { AllLineItems = lineItems },
                TransmissionDateTime = DateTime.UtcNow.ToString("yyyyMMddhhmmss"),

            };
            return requestBody;

        }

        public string PrepareQueryString( PaymentStatusRequest requestBody)
        {
            string queryParams = "?";
            int i = 0;

            var property = requestBody.GetType().GetProperties();

            foreach(var prop in property)
            {
                if(i>0) queryParams+="&";
                var jsonProperty = prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName;
                queryParams +=(jsonProperty+"="+prop.GetValue(requestBody,null));
                i++;
            }
            return queryParams;

        }

        //public List<string> PrepareQCardProducts(Order orders)
        //{
        //    var productCodes = _productService.GetPro
        //}
    }
}
