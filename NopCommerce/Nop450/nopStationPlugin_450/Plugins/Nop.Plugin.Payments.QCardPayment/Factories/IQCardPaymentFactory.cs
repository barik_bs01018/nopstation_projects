﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Models;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Factories
{
    public interface IQCardPaymentFactory
    {
        Task<PaymentUrlRequest> PreparePaymentUrlRequest(QCardPaymentSettings settings, Order orders);
        string PrepareQueryString(PaymentStatusRequest requestBody);
    }
}