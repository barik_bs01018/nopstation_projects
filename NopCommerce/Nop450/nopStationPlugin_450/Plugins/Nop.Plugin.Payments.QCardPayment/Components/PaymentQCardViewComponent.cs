﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Components
{
    public class PaymentQCardViewComponent:NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Payments.QCardPayment/Views/PaymentInfo.cshtml");
        }
    }
}
