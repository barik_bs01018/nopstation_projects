﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment
{
    public static class QCardPaymentDefaults
    {
        public static string ACCEPT_SUCCESSFUL => "00"; //The => syntax is equal to the get { return ... } syntax.

        public static string NOT_VALID => "202";    // String Property { get; } = "value";

        public static string SYSTEM_ERROR => "203";

        public static string INVALID_USER_DETAIL => "204";

        public static string ACCESS_NOT_ALLOWED => "209";
        public static string INVALID_PRODUCT_CODES => "257";

        public static string APPROVED => "00";
        public static string DECLINED => "01";
        public static string IN_PROGRESS => "02";
        public static string ERROR => "03";


        public static string SAND_BOX_REQUEST_PAYMENT_URL => "https://api.flexilongtermfinance.co.nz/api/simulator/gateway/rest/v1/payment/paymenturl";
        public static string SAND_BOX_RESPONSE_PAYMENT_STATUS_URL => "https://api.flexilongtermfinance.co.nz/api/simulator/gateway/rest/v1/payment/paymentstatus";


        public static string BASE_REQUEST_PAYMENT_URL => "https://api.flexilongtermfinance.co.nz/api/gateway/rest/v1/payment/paymenturl";
        public static string BASE_RESPONSE_PAYMENT_STATUS_URL => "https://api.flexilongtermfinance.co.nz/api/gateway/rest/v1/payment/paymentstatus";

    }
}
