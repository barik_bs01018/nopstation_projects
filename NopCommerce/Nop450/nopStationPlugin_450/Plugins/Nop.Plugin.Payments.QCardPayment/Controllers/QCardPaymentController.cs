﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Factories;
using Nop.Plugin.Payments.QCardPayment.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Controllers
{
    public class QCardPaymentController: BasePaymentController
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly IOrderService _oderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger _logger;
        private readonly IQCardPaymentFactory _qCardPaymentFactory;


        public QCardPaymentController(ILocalizationService localizationService,
            ISettingService settingService,
            IWebHelper webHelper,
            IStoreContext storeContext,
            IOrderService orderService,
            IHttpContextAccessor httpContextAccessor,
            IOrderProcessingService orderProcessingService,
            ILogger logger,
            IQCardPaymentFactory qCardPaymentFactory)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _oderService = orderService;
            _httpContextAccessor = httpContextAccessor;
            _orderProcessingService = orderProcessingService;
            _logger = logger;
            _qCardPaymentFactory = qCardPaymentFactory;
        }

        public async Task<IActionResult> PostPaymentHandler(string merchant_transaction_id, string process_no)
        {
            var orderId = Convert.ToInt32(merchant_transaction_id);
            var order = await _oderService.GetOrderByIdAsync(orderId);
            if(order == null)
            {
                return RedirectToRoute("Homepage");
            }
            var storeScope = _storeContext.GetCurrentStore().Id;
            var settings = await _settingService.LoadSettingAsync<QCardPaymentSettings>(storeScope);
            var baseUrl = settings.UseSandbox ? QCardPaymentDefaults.SAND_BOX_RESPONSE_PAYMENT_STATUS_URL : QCardPaymentDefaults.BASE_RESPONSE_PAYMENT_STATUS_URL;

            var requestBody = new PaymentStatusRequest()
            {
                MerchantId = settings.MerchantId,
                LoginId = settings.LoginId,
                Password = settings.Password,
                ApiKey = settings.ApiKey,
                MerchantTransactionId = merchant_transaction_id,
                ProcessNo = process_no,
                TransmissionDateTime = DateTime.UtcNow.ToString("yyyyMMddhhmmss")
            };
            string queryParams=_qCardPaymentFactory.PrepareQueryString(requestBody); 
            var request = WebRequest.Create($"{baseUrl+queryParams}");
            request.Method = "GET";
            
            try
            {
                var webResponse = request.GetResponse();
                using var webStream = webResponse.GetResponseStream() ?? Stream.Null;
                using var responseReader = new StreamReader(webStream);
                var response = responseReader.ReadToEnd();
                var paymentStatusResponse = JsonConvert.DeserializeObject<PaymentStatusResponse>(response);

                var paymentStatusCode = paymentStatusResponse.Payment_Status.StatusCode;
                var paymentStatusDesc =paymentStatusResponse.Payment_Status.StatusDesc ;

                var resultCode =paymentStatusResponse.Result.ResultCode;
                var resultDesc =paymentStatusResponse.Result.ResultDesc;

                if(resultCode.Equals(QCardPaymentDefaults.APPROVED)&&
                    paymentStatusCode.Equals(QCardPaymentDefaults.APPROVED))
                {
                    await _oderService.InsertOrderNoteAsync(new OrderNote()
                    {
                        OrderId = order.Id,
                        Note = $"Order Payment Completed. Transaction Id : {process_no}",
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow

                    });
                    order.CaptureTransactionId = process_no;
                    order.OrderStatus=OrderStatus.Pending;
                    await _oderService.UpdateOrderAsync(order);
                    await _orderProcessingService.MarkOrderAsPaidAsync(order);
                    return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
                }
                else
                {
                    await _oderService.InsertOrderNoteAsync(new OrderNote()
                    {
                        OrderId = order.Id,
                        Note = $"Order Payment Failed!. Transaction Id : {process_no}",
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow

                    });
                    return RedirectToRoute("OrderDetails", new { orderId = order.Id });
                }

            }
            catch (Exception ex)
            {
              await _logger.InformationAsync(ex.Message);
            }
            return RedirectToRoute("Homepage");
        }
    }
}
