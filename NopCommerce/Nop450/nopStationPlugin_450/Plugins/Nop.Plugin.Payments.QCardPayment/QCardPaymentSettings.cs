﻿using Newtonsoft.Json;
using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment
{
    public class QCardPaymentSettings:ISettings
    {

        
        public bool UseSandbox { get; set; }

        [JsonProperty("merchant_id")]
        public string MerchantId { get; set; }

       
        [JsonProperty("login_id")]
        public string LoginId { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("api_key")]
        public string ApiKey { get; set; }

        public decimal AdditionalFee { get; set; }

        public bool AdditionalFeePercentage { get; set; }

    }
}
