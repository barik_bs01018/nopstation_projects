﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Models
{
    public class PaymentStatusResponse
    {
        [JsonProperty("result")]
        public PaymentResult Result { get; set; }

        [JsonProperty("paymentStatus")]
        public PaymentStatus Payment_Status { get; set; }

        [JsonProperty("retrieval_reference_number")]
        public string Retrieval_Reference_Number { get; set; }

        [JsonProperty("selected_product_code")]
        public string Selected_Product_Code { get; set; }

        [JsonProperty("merchant_transaction_id")]
        public string Merchant_Transaction_Id { get; set; }

        [JsonProperty("transmission_date_time")]
        public DateTime TransmissionDateTime { get; set; }


    }
}
