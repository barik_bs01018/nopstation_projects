﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Models
{
    public class PaymentResult
    {
        [JsonProperty("resultCode")]
        public string ResultCode { get; set; }

        [JsonProperty("resultDesc")]
        public string ResultDesc { get; set; }
    }
}
