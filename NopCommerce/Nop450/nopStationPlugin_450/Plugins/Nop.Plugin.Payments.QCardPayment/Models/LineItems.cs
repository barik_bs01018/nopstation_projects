﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment.Models
{
    public class LineItems
    {
        [JsonProperty("lineItem")]
        public List<LineItem> AllLineItems { get; set; }
    }
}
