﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Factories;
using Nop.Plugin.Payments.QCardPayment.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.QCardPayment
{
    public class QCardPaymentProcessor: BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly IQCardPaymentFactory _qCardPaymentFactory;
        private readonly IOrderService _oderService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger _logger;

        private readonly QCardPaymentSettings _qCardPaymentSettings;

        #endregion

        #region Ctor

        public QCardPaymentProcessor(ILocalizationService localizationService,
            ISettingService settingService,
            IWebHelper webHelper,
            IStoreContext storeContext,
            IQCardPaymentFactory qCardPaymentFactory,
            IOrderService orderService,
            IOrderTotalCalculationService orderTotalCalculationService,
            IHttpContextAccessor httpContextAccessor,
            ILogger logger,
            QCardPaymentSettings qCardPaymentSettings)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
            _storeContext = storeContext;   
            _qCardPaymentFactory=qCardPaymentFactory;
            _oderService = orderService;
            _orderTotalCalculationService = orderTotalCalculationService;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            _qCardPaymentSettings = qCardPaymentSettings;
        }

        #endregion

        #region Methods

        public Task<ProcessPaymentResult> ProcessPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult
            {
                AllowStoringCreditCardNumber = true
            };
          
            return Task.FromResult(result);
        }

       
        public async Task PostProcessPaymentAsync(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var storeScope = _storeContext.GetCurrentStore().Id;
            var settings = await _settingService.LoadSettingAsync<QCardPaymentSettings>(storeScope);
            var baseUrl = settings.UseSandbox? QCardPaymentDefaults.SAND_BOX_REQUEST_PAYMENT_URL : QCardPaymentDefaults.SAND_BOX_RESPONSE_PAYMENT_STATUS_URL;
            
            var request = WebRequest.Create($"{baseUrl}");
            request.Method = "POST";
            request.ContentType = "application/json";

            var order = postProcessPaymentRequest.Order;
            var requestBody = await _qCardPaymentFactory.PreparePaymentUrlRequest(settings,order);

            var json = JsonConvert.SerializeObject(requestBody);
            request.ContentLength = json.Length;

            using(var webStream = request.GetRequestStream())
            {
                using var requestWriter = new StreamWriter(webStream, Encoding.ASCII);
                requestWriter.Write(json);
            }
            try
            {
                var webResponse = request.GetResponse();
                using var webStream = webResponse.GetResponseStream()?? Stream.Null;
                using var responseReader = new StreamReader(webStream);
                var response = responseReader.ReadToEnd();
                var paymentUrlresponse = JsonConvert.DeserializeObject<PaymentUrlResponse>(response);

                if(paymentUrlresponse.Result.ResultCode== QCardPaymentDefaults.ACCEPT_SUCCESSFUL)
                {
                    var orderId = paymentUrlresponse.MerchantTransactionId;
                    var redirectUrl = paymentUrlresponse.PaymentUrl;
                    var processNo = paymentUrlresponse.ProcessNo;
                    order.AuthorizationTransactionId = processNo;
                    await _oderService.UpdateOrderAsync(order);
                    _httpContextAccessor.HttpContext.Response.Redirect(redirectUrl);
                    
                }
                else
                {
                    await _logger.InformationAsync(LogLevel.Error.ToString());

                }


            }
            catch (Exception ex)
            {
                await _logger.InformationAsync(ex.Message);
            }
        }

        
        public Task<bool> HidePaymentMethodAsync(IList<ShoppingCartItem> cart)
        {
            
            return Task.FromResult(false);
        }

     
        public async Task<decimal> GetAdditionalHandlingFeeAsync(IList<ShoppingCartItem> cart)
        {
            return await _orderTotalCalculationService.CalculatePaymentAdditionalFeeAsync(cart,
                _qCardPaymentSettings.AdditionalFee, _qCardPaymentSettings.AdditionalFeePercentage);
        }

   
        public Task<CapturePaymentResult> CaptureAsync(CapturePaymentRequest capturePaymentRequest)
        {
            return Task.FromResult(new CapturePaymentResult { Errors = new[] { "Capture method not supported" } });
        }

        
        public Task<RefundPaymentResult> RefundAsync(RefundPaymentRequest refundPaymentRequest)
        {
            return Task.FromResult(new RefundPaymentResult { Errors = new[] { "Refund method not supported" } });
        }

     
        public Task<VoidPaymentResult> VoidAsync(VoidPaymentRequest voidPaymentRequest)
        {
            return Task.FromResult(new VoidPaymentResult { Errors = new[] { "Void method not supported" } });
        }

        
        public Task<ProcessPaymentResult> ProcessRecurringPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult
            {
                AllowStoringCreditCardNumber = true
            };
          

            return Task.FromResult(result);
        }

        
        public Task<CancelRecurringPaymentResult> CancelRecurringPaymentAsync(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            //always success
            return Task.FromResult(new CancelRecurringPaymentResult());
        }

   
        public Task<bool> CanRePostProcessPaymentAsync(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return Task.FromResult(false);
        }

      
        public Task<IList<string>> ValidatePaymentFormAsync(IFormCollection form)
        {
            var warnings = new List<string>();

            

            return Task.FromResult<IList<string>>(warnings);
        }

  
        public Task<ProcessPaymentRequest> GetPaymentInfoAsync(IFormCollection form)
        {
            return Task.FromResult(new ProcessPaymentRequest());
        }

      
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/QCardPaymentConfigure/Configure";
        }

     
        public string GetPublicViewComponentName()
        {
            return "PaymentQCard";
        }

    
        public override async Task InstallAsync()
        {

            await _settingService.SaveSettingAsync(new QCardPaymentSettings
            {
                UseSandbox = true,
                ApiKey = "2bef9740cd0be5995e58f9eef3249cabbd65d4bc",
                MerchantId = "12321",
                LoginId = "203399",
                Password = "Password01"
                
            });

            //locales
            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Payments.QCardPayment.Fields.UseSandbox"] = "Use Sandbox",
                ["Plugins.Payments.QCardPayment.Fields.UseSandbox.Hint"] = "Check Sandbox for checking procedure of QCard Payment",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFee"] = "Additional Fees",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFee.Hint"] = "Enter additional fee to charge your customers.",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFeePercentage"] = "Additional fee. Use percentage",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFeePercentage.Hint"] = "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.",
                ["Plugins.Payments.QCardPayment.Fields.MerchantId"] = "Merchant Id",
                ["Plugins.Payments.QCardPayment.Fields.MerchantId.Hint"] = "Use your merchant Id provided form QCard",
                ["Plugins.Payments.QCardPayment.Fields.LoginId"] = "LogIn ID",
                ["Plugins.Payments.QCardPayment.Fields.LoginId.Hint"] = "Use your Log In Id provided form QCard",
                ["Plugins.Payments.QCardPayment.Fields.Password"] = "Password",
                ["Plugins.Payments.QCardPayment.Fields.Password.Hint"] = "Use your password provided form QCard. Password is Required",
                ["Plugins.Payments.QCardPayment.Fields.ApiKey"] = "API Key",
                ["Plugins.Payments.QCardPayment.Fields.ApiKey.Hint"] = "Use your Api Key provided form QCard. ApiKey is Required",
                
            });

            await base.InstallAsync();
        }

      
        public override async Task UninstallAsync()
        {
            
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Payments.QCardPayment");

            await base.UninstallAsync();
        }


        public async Task<string> GetPaymentMethodDescriptionAsync()
        {
            return await _localizationService.GetResourceAsync("Plugins.Payments.Manual.PaymentMethodDescription");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture => false;

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund => false;

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund => false;

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid => false;

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.Manual;

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType => PaymentMethodType.Redirection;

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo => false;

        #endregion

    }
}